//-----------------------------LICENSE NOTICE------------------------------------
//  This file is part of CPCtelera: An Amstrad CPC Game Engine
//  Copyright (C) 2015 ronaldo / Fremos / Cheesetea / ByteRealms (@FranGallegoBR)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <types.h>
//CODIFICACION
// POSICIONES
// Bit 0,1 Arriba | Bit 2,3 Izquierda | Bit 4,5 Derecha | Bit 6,7 Abajo
// PAREDES Y PUERTA
// Bit 0,2,4,6 Paredes Bit 1,3,5,7 Puertas || 1 HAY , 0 NO HAY
//EJEMPLO
//###############
//#
//P
//#
//###############
// map = {0b 10 11 00 10}
// Hay pared arriba pero no puerta 10
// Hay pared y puerta izquierda 11
// No hay pared ni puerta derecha 00
// Hay pared abajo pero no puerta 10


//BLOQUE 1 CONECTOR                     BLOQUE 2 PASILLO VERTICAL
/*
########P#########                      ################
#                #                      #              #
P                P                      P              P
#                #                      #              #
########P#########                      ################
*/
u8 map_model1 = {0b11111111};           u8 map_model2[2] = {0b10111110};
//BLOQUE 3 PASILLO HORIZONTAL           BLOQUE 4 ESQUINA SUPERIOR-IZQUIERDA
/*
########P#########                      ################
#                #                      #              #
#                #                      #              P
#                #                      #              #
########P#########                      #######P########
*/
u8 map_model1 = {0b10111110};           u8 map_model2[2] = {0b10111110};
